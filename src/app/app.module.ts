import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { DataHandlerService } from './service/data-handler.service';
import { TaskComponent } from './views/tasks/tasks.component';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './views/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SignAppComponent } from './views/sign-app/sign-app.component';
import { KeyValuePipe } from '@angular/common';
import { NavigateComponent } from './views/navigate/navigate.component';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { TablesComponent } from './views/tables/tables.component';
import { NotificationComponent } from './views/notification/notification.component';
import { ProfileComponent } from './views/profile/profile.component';
import { AppRoutingModule } from './app-routing.module';
import { EditTaskComponent } from './views/edit-task/edit-task.component';
import { AddProjectComponent } from './views/add-project/add-project.component';

const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'sign-app', component: SignAppComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'tables', component: TablesComponent },
  { path: 'notification', component: NotificationComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'edit-task', component: EditTaskComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignAppComponent,
    TaskComponent,
    NavigateComponent,
    DashboardComponent,
    TablesComponent,
    NotificationComponent,
    ProfileComponent,
    EditTaskComponent,
    AddProjectComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [DataHandlerService,
              KeyValuePipe],
  bootstrap: [AppComponent]
})
export class AppModule {}
