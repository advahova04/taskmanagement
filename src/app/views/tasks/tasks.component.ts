import { Component, OnInit } from "@angular/core";
import { AuthService } from "src/app/service/auth.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TaskComponent implements OnInit {
  tasksArray: any[] = [];
  selectedProjectId: string | undefined;

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit() {
    this.getTasks();
  }

  getTasks() {
    this.authService.getTasks().subscribe(
      (response: any) => {
        this.tasksArray = response.data;
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  getTasksByProjectId(projectId: string) {
    this.authService.getTasksByProjectId(projectId).subscribe(
      (response: any) => {
        this.tasksArray = response.data;
      },
      (error: any) => {
        console.error(error);
      }
    );
  }

  selectProject(projectId: string) {
    this.selectedProjectId = projectId;
    this.getTasksByProjectId(projectId);
  }

  confirmDeleteTask(index: number) {
    if (confirm("Вы уверены, что хотите удалить эту задачу?")) {
      this.authService.deleteTask(this.tasksArray[index].id).subscribe(
        (response: any) => {
          this.tasksArray.splice(index, 1);
        },
        (error: any) => {
          console.error(error);
        }
      );
    }
  }
  
  editedTask: any = {};
  editingIndex: number = -1;

  editTask(index: number) {
    this.editedTask = { ...this.tasksArray[index] };
    this.editingIndex = index;
    this.router.navigate(['/edit-task'], { queryParams: { taskData: JSON.stringify(this.editedTask) } })
      .then((result: boolean) => {
        if (!result) {
          console.error('Failed to navigate to edit-task page.');
        }
      })
      .catch((error: any) => {
        console.error('Error navigating to edit-task page:', error);
      });
  }

}