import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigate',
  templateUrl: './navigate.component.html',
  styleUrls: ['./navigate.component.css']
})
export class NavigateComponent {
  constructor(private router: Router) {}

  isActive(url: string): boolean {
    return this.router.isActive(url, true);
  }
  
}
