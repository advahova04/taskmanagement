import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from "src/app/service/auth.service";
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-task',
  templateUrl: './edit-task.component.html',
  styleUrls: ['./edit-task.component.css']
})
export class EditTaskComponent implements OnInit {
  editedTask: any = {};
  editingIndex: number = -1;

  constructor(private router: Router, private authService: AuthService, private route: ActivatedRoute) {}
  
  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      const taskData = params['taskData'];
      this.editedTask = JSON.parse(taskData);
    });
  }

  
  saveTask() {
    this.authService.updateTask(this.editedTask.id, this.editedTask).subscribe(
      (response: any) => {
        // Обработка успешного сохранения
        this.editingIndex = -1;
        this.router.navigate(["/tasks"]);
      },
      (error: any) => {
        console.error(error);
        // Обработка ошибки сохранения
      }
    );
  }

  cancelEdit() {
    this.editingIndex = -1;
    this.router.navigate(["/tasks"]);
  }

  handleFileInput(event: any) {
    const files: FileList = event.target.files;
  
    // Обработка загруженных файлов
    for (let i = 0; i < files.length; i++) {
      const file: File = files[i];
      console.log('Файл:', file);
  
      // Выполнение необходимых операций с файлом
      // Например, можно выводить название и размер файла
      console.log('Название файла:', file.name);
      console.log('Размер файла:', file.size);
    }
  }
}
