import { Component } from '@angular/core';
import { ProjectService } from 'src/app/service/project.service';

@Component({
  selector: 'app-tables',
  templateUrl: './tables.component.html',
  styleUrls: ['./tables.component.css']
})
export class TablesComponent {
  projectsArray: any[] | undefined;
  
  constructor(private projectService: ProjectService) {}

  ngOnInit() {
    this.getProjects();
  }

  getProjects() {
    this.projectService.getProjects().subscribe(
      (response: any) => {
        this.projectsArray = response.data;
      },
      (error: any) => {
        console.error(error);
      }
    );
  }


  getRandomCompletion(): number {
    const randomCompletion = Math.floor(Math.random() * 11) * 10;
    return randomCompletion;
  }

  public imagePaths: string[] = [
    '../assets/img/small-logos/logo-asana.svg',
    '../assets/img/small-logos/github.svg',
    '../assets/img/small-logos/logo-atlassian.svg',
    '../assets/img/small-logos/bootstrap.svg',
    '../assets/img/small-logos/logo-slack.svg',
    '../assets/img/small-logos/devto.svg'
  ];

  getRandomImage(): string {
    const randomIndex = Math.floor(Math.random() * this.imagePaths.length);
    return this.imagePaths[randomIndex];
  }

  getRandomBudget(): string {
    const min = 1000;
    const max = 10000;
    const randomBudget = (Math.random() * (max - min) + min).toFixed(2);
    const formattedBudget = Number(randomBudget).toLocaleString();
    return `$${formattedBudget}`;
  }
}
