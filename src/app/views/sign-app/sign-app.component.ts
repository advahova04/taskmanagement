import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-sign-app',
  templateUrl: './sign-app.component.html',
  styleUrls: ['./sign-app.component.css']
})
export class SignAppComponent implements OnInit, OnDestroy{

  form: FormGroup = new FormGroup({
    email: new FormControl(null, [Validators.required, Validators.email]),
    password: new FormControl(null, [Validators.required, Validators.minLength(4)])
  });
  aSub: Subscription = new Subscription();

  constructor(private authService: AuthService, private router: Router, private route: ActivatedRoute) {}

  ngOnDestroy() {
    if(this.aSub){
      this.aSub.unsubscribe()
    }
  }

  ngOnInit(){
    this.form = new FormGroup(
      {
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required, Validators.minLength(4)])
    })
  }

  onSubmit(){
    this.form.disable();

    this.aSub = this.authService.register(this.form.value).subscribe(
      () => {
        this.router.navigate(['/login'], {
          queryParams: {
            registered: true
          }
        })
      },
      error => {
        console.warn(error);
        this.form.enable();
      }
    )
  }
}
