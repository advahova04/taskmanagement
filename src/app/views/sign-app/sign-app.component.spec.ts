import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SignAppComponent } from './sign-app.component';

describe('SignAppComponent', () => {
  let component: SignAppComponent;
  let fixture: ComponentFixture<SignAppComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SignAppComponent]
    });
    fixture = TestBed.createComponent(SignAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
