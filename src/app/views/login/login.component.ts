import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit, OnDestroy{
  form: FormGroup = new FormGroup({
    email: new FormControl(null, [Validators.required, Validators.email]),
    password: new FormControl(null, [Validators.required, Validators.minLength(4)])
  });
  aSub: Subscription = new Subscription();

  constructor(private authService: AuthService, private router: Router, private route: ActivatedRoute) {}
  
  ngOnDestroy() {
    if(this.aSub){
      this.aSub.unsubscribe()
    }
  }

  ngOnInit(){
    this.form = new FormGroup(
      {
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required, Validators.minLength(4)])
    })

    this.route.queryParams.subscribe((params: Params) => {
      if(params['registered']){

      } else if(params['accessDenied']){

      }
    })
  }

  onSubmit(){
    this.form.disable();

    this.aSub = this.authService.login(this.form.value).subscribe(
      (response) => {
        console.log('Token passed:', response.token); 
        this.router.navigate(['/tasks']);
      },
      error => {
        console.warn(error);
        this.form.enable();
      }
    )
  }
}