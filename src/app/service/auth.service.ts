import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, pipe, tap } from 'rxjs';
import { User } from '../views/interface';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  private token: string | null = null;

  private apiUrl = 'http://localhost:8000/api';

  deleteTask(taskId: string): Observable<any> {
    const token = '99|6z9sKGZhAeFHuC0GYOpTJBze3c1iezJ3cJuwjBvk';
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`
    });
    const url = `${this.apiUrl}/tasks/${taskId}`; 
    return this.http.delete(url, { headers });
  }

  updateTask(taskId: string, updatedTask: any): Observable<any> {
    const token = '99|6z9sKGZhAeFHuC0GYOpTJBze3c1iezJ3cJuwjBvk';
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`
    });
    const url = `${this.apiUrl}/tasks/${taskId}`; 
    return this.http.put(url, updatedTask, { headers });
  }  
  
  getTasksByProjectId(projectId: string): Observable<any> {
    return this.http.get(`/api/projects/${projectId}/tasks`);
  }

  constructor(private http: HttpClient) { }

    register(user: User): Observable<User> {
    return this.http.post<User>(`${this.apiUrl}/register`, user);
  }

  getTasks(): Observable<any> {
    const token = '99|6z9sKGZhAeFHuC0GYOpTJBze3c1iezJ3cJuwjBvk';
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`
    });
  
    return this.http.get<any>('http://localhost:8000/api/tasks', { headers });
  }
  

  login(user: User): Observable<{token: string}> {
    return this.http.post<{token: string}>(`${this.apiUrl}/login`, user)
      .pipe(
        tap(({token}) => {
          console.log('Token:', token); // Проверьте значение токена здесь
          localStorage.setItem('auth-token', token);
          this.setToken(token);
        })
      );
  }
  
  setToken(token: string | null){
    this.token = token;
  }

  getToken(): string | null{
    return this.token;
  }

  isAuthenticated(): boolean{
    return !!this.token;
  }

  logout() {
    this.setToken(null);
    localStorage.clear();
  }
}
