import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class DataHandlerService {

    constructor(private httpClient: HttpClient) { }

    fillTask(headers: HttpHeaders) {
        return this.httpClient.get('http://127.0.0.1:8000/api/tasks', { headers });
      }
}
