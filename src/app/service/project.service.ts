import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  private apiUrl = 'http://127.0.0.1:8000/api';

  constructor(private http: HttpClient) { }

  getProjects(): Observable<any> {
    const token = '99|6z9sKGZhAeFHuC0GYOpTJBze3c1iezJ3cJuwjBvk';
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`
    });
  
    return this.http.get<any>('http://localhost:8000/api/projects', { headers });
  }
}