import { Category } from "./Category";
import { Priority } from "./Priority";
import { Status } from "./Status";

export class Task{
    id: number;
    title: string;
    status?: Status;
    priority?: Priority;
    category?: Category;
    date?: Date;

    constructor(id: number, title: string, status?: Status, priority?: Priority, category?: Category, date?: Date){
        this.id = id;
        this.title = title;
        this.status = status;
        this.priority = priority;
        this.category = category;
        this.date = date;
    }
}